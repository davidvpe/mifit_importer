const datos = require('./datos.json')

let datosLimpios = datos.filter((registro) => registro.metabolism > 0)

datosLimpios = datosLimpios.map((registro) => {
    let { timestamp, ...rest } = registro

    rest.date = new Date(timestamp * 1000).toLocaleString('en-US', {
        dateStyle: 'full',
        timeStyle: 'full',
    })

    return rest
})

const express = require('express')

let app = express()

app.get('', (req, res) => {
    res.json(datosLimpios)
})

app.listen(5000, () => {
    console.log('Listening on port 5000')
})
